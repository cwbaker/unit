
#include <unit/UnitTest.h>
#include <unit/TestReporterStdout.h>

int main(int, char const *[])
{
    return UnitTest::RunAllTests();
}
